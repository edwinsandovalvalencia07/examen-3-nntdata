package com.nttdata.ejsv.banco.dto.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BancoResponseDto {
    private Integer id;
    private String nombre;
    private String direccion;
    private Boolean active;


}
