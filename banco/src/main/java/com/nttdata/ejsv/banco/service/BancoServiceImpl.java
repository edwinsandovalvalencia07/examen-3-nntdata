package com.nttdata.ejsv.banco.service;


import com.nttdata.ejsv.banco.dto.request.BancoRequestDto;
import com.nttdata.ejsv.banco.dto.response.BancoResponseDto;
import com.nttdata.ejsv.banco.entity.Banco;
import com.nttdata.ejsv.banco.repository.IBancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BancoServiceImpl implements IBancoService {
    @Autowired()
    private IBancoRepository iBancoRepository;
    @Override
    public List<BancoResponseDto> listAll() {
        return this.iBancoRepository.findAll().stream()
                .map(p -> {
                    BancoResponseDto responseDto = new BancoResponseDto();
                    responseDto.setId(p.getId());
                    responseDto.setNombre(p.getNombre());
                    responseDto.setDireccion(p.getDireccion());
                    responseDto.setActive(p.getActive());

                    return responseDto;
                }).collect(Collectors.toList());
    }

    @Override
    public BancoResponseDto save(BancoRequestDto request) {
        Banco banco=new Banco();
        banco.setId(request.getId());
        banco.setNombre(request.getNombre());
        banco.setDireccion(request.getDireccion());
        banco.setActive(request.getActive());
        this.iBancoRepository.save(banco);

        BancoResponseDto responseDto = new BancoResponseDto();
        responseDto.setId(banco.getId());
        responseDto.setNombre(banco.getNombre());
        responseDto.setDireccion(banco.getDireccion());
        responseDto.setActive(banco.getActive());
        return responseDto;
    }

    @Override
    public BancoResponseDto update(BancoRequestDto request, Integer id) {
        Banco banco=new Banco();
//        banco.setId(request.getId());
        banco.setNombre(request.getNombre());
        banco.setDireccion(request.getDireccion());
        banco.setActive(request.getActive());
        this.iBancoRepository.save(banco);

        BancoResponseDto responseDto = new BancoResponseDto();
//        responseDto.setId(banco.getId());
        responseDto.setNombre(banco.getNombre());
        responseDto.setDireccion(banco.getDireccion());
        responseDto.setActive(banco.getActive());
        return responseDto;


    }

    @Override
    public String eliminar(Integer id) {
        Optional<Banco> encontrado = iBancoRepository.findById(id);
        if (encontrado.isPresent()) {
            iBancoRepository.deleteById(id);
            return "Banco eliminado correctamente";
        }
        return "Banco no se encuentra registrado";
    }

//    @Override
//    public List<BancoResponseDto> listBancoNombre(String nombre) {
//        return this.iBancoRepository.findByNombresLike(nombre).stream()
//                .map(p -> {
//                    BancoResponseDto empleadoResponseDto = new BancoResponseDto();
//                    empleadoResponseDto.setId(p.getIdBanco());
//                    empleadoResponseDto.setNombre(p.getNombre());
//
//
//
//                    return empleadoResponseDto;
//                }).collect(Collectors.toList());
//    }
}
