package com.nttdata.ejsv.banco.service;



import com.nttdata.ejsv.banco.dto.request.BancoRequestDto;
import com.nttdata.ejsv.banco.dto.response.BancoResponseDto;

import java.util.List;

public interface IBancoService {
    List<BancoResponseDto> listAll();

    BancoResponseDto save(BancoRequestDto request);

    BancoResponseDto update(BancoRequestDto request,Integer id);

    String eliminar(Integer id);
//    List<BancoResponseDto> listBancoNombre(String nombre);
}
