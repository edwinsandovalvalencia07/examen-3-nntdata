package com.nttdata.ejsv.cliente.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClienteResponseDto {
//    private Integer id;
    private Integer id;
    private String nombres;
    private String apellidos;
    private String sexo;
    private Boolean active;


}
