package com.nttdata.ejsv.serviceusuario.repository;

import com.nttdata.ejsv.serviceusuario.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Integer> {

    @Query(value = "select  * from usuario where active = ?1", nativeQuery = true)
    List<Usuario> listUsuarioActive(Boolean active);
}
