package com.nttdata.ejsv.serviceusuario.exceptionhandler.exception;

public class ExisteDepartamento extends RuntimeException{
    public ExisteDepartamento(String message) {
        super(message);
    }
}
