package com.nttdata.ejsv.serviceusuario.exceptionhandler.exception;

public class NoExisteDepartamentoActualizar extends RuntimeException{
    public NoExisteDepartamentoActualizar(String message) {
        super(message);
    }
}
