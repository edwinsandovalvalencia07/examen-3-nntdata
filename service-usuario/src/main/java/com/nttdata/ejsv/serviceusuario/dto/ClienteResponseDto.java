package com.nttdata.ejsv.serviceusuario.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClienteResponseDto implements Serializable {
    private Integer id;
    private String nombres;
    private String apellidos;
    private String sexo;
    private Boolean active;
    private String mensaje;

    public ClienteResponseDto(String mensaje) {
        this.mensaje = mensaje;
    }
}
