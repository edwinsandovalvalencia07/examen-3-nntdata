package com.nttdata.ejsv.serviceusuario.utils;

public enum Roles {
    ADMIN(1, "Administrador"),
    GERENTE(2, "Gerente"),
    INVITADO(3, "Invitado"),
    CLIENTE(4,"Cliente");

    private int id;
    private String name;

    Roles(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public static String getRoleById(int id) {
        for (Roles role : values()) {
            if (role.id == id) {
                return role.name;
            }
        }
        return null; // Devolver null si no se encuentra el ID
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
