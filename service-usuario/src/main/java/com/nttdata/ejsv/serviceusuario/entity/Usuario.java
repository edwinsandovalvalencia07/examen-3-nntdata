package com.nttdata.ejsv.serviceusuario.entity;

//import jakarta.persistence.*;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "usuario")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String usuario;
    private String clave;
    private Boolean active;
    private Integer idCliente;
    @ManyToOne(cascade = CascadeType.MERGE, fetch =
            FetchType.EAGER)
    @JoinColumn(name = "idRol")//es el id que tiene la entidad rol
    private Rol rol;


}
