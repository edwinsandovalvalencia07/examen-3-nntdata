package com.nttdata.ejsv.serviceusuario.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "rol")
@NoArgsConstructor//constructor vacio
@AllArgsConstructor//constructor lleno
@Getter
@Setter
public class Rol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idRol")
    private Integer id;

    @Column()
    private String nombre;

    @Column
    private String descripcion;

    @OneToMany(mappedBy = "rol")//la variable q esta dentro es la que se creo en la entidad usuario
    private List<Usuario> usuarios;

    public Rol(Integer id) {
        this.id = id;
    }

    public Rol(Integer id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
}
