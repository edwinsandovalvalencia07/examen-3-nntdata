package com.nttdata.ejsv.serviceusuario.service;

import com.nttdata.ejsv.serviceusuario.dto.request.UsuarioRequestDto;
import com.nttdata.ejsv.serviceusuario.dto.respose.UsuarioResponseDto;

import java.util.List;

public interface IUsuarioService {
    List<UsuarioResponseDto> listAll();

    UsuarioResponseDto save(UsuarioRequestDto request);

    UsuarioResponseDto update(UsuarioRequestDto request,Integer id);

    String eliminar(Integer id);

    UsuarioResponseDto getUsuarioById (Integer id);
    List<UsuarioResponseDto> listarUsuarioActivo(Boolean active);

}
