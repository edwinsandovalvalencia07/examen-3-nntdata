package com.nttdata.ejsv.serviceusuario.controller;


import com.nttdata.ejsv.serviceusuario.dto.request.UsuarioRequestDto;
import com.nttdata.ejsv.serviceusuario.dto.respose.UsuarioResponseDto;
import com.nttdata.ejsv.serviceusuario.service.IUsuarioService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

//    @GetMapping("/listar")
//    public String saludo(){
//        return "hola como estas";
//    }
    @Autowired
    private IUsuarioService iUsuarioService;

    @Operation(summary = "Listar todos los Usuarios")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content = @Content),
    })
    @GetMapping("/listar")
    public ResponseEntity<List<UsuarioResponseDto>> listAll(){
        return  ResponseEntity.ok(iUsuarioService.listAll());
    }

    @Operation(summary = "Listar todos los Usuarios segun el estado active")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content = @Content),
    })
    @GetMapping("/listarActive")
    public ResponseEntity<List<UsuarioResponseDto>> listarUsuarioActivo(@RequestParam(name = "active") Boolean active){
        return  ResponseEntity.ok(iUsuarioService.listarUsuarioActivo(active));
    }

    @Operation(summary = "Listar Usuario por Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content = @Content),
    })
    @GetMapping("/listar/{id}")
    public ResponseEntity<UsuarioResponseDto> buscarUsuario(@PathVariable(name = "id") Integer idUsuario){
        return new ResponseEntity<>(this.iUsuarioService.getUsuarioById(idUsuario), HttpStatus.OK);
    }


    @Operation(summary = "Guardar Usuario")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Create", content = @Content),
    })
    @PostMapping("/guardar")
    public ResponseEntity<?> save(@RequestBody UsuarioRequestDto request){
        return new ResponseEntity<>(this.iUsuarioService.save(request), HttpStatus.CREATED);
    }


    @Operation(summary = "Listar todos los Usuarios")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Accepted", content = @Content),
    })
    @DeleteMapping("/eliminar/{id}")
    public String delete(@PathVariable("id") Integer id){
        return iUsuarioService.eliminar(id);
    }


    @Operation(summary = "Listar todos los Usuarios")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content = @Content),
    })
    @PutMapping("/actualizar/{id}")
    public UsuarioResponseDto actualizar(@RequestBody UsuarioRequestDto body, @PathVariable("id") Integer idUsuario) {
        return iUsuarioService.update(body, idUsuario);
    }
}
