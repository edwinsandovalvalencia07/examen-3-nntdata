package com.nttdata.ejsv.serviceusuario.exceptionhandler.exception;

public class NoExisteEmpleadoActualizar extends RuntimeException{
    public NoExisteEmpleadoActualizar(String message) {
        super(message);
    }
}
