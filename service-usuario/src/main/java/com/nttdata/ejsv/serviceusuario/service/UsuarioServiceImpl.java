package com.nttdata.ejsv.serviceusuario.service;


import com.nttdata.ejsv.serviceusuario.config.IClienteFeing;
import com.nttdata.ejsv.serviceusuario.dto.ClienteResponseDto;
import com.nttdata.ejsv.serviceusuario.dto.request.UsuarioRequestDto;
import com.nttdata.ejsv.serviceusuario.dto.respose.RolResponseDto;
import com.nttdata.ejsv.serviceusuario.dto.respose.UsuarioResponseDto;
import com.nttdata.ejsv.serviceusuario.entity.Rol;
import com.nttdata.ejsv.serviceusuario.entity.Usuario;
import com.nttdata.ejsv.serviceusuario.repository.IUsuarioRepository;
import com.nttdata.ejsv.serviceusuario.utils.Roles;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
//@RequiredArgsConstructor
@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private  IClienteFeing iClienteFeing;
    @Autowired()
    private IUsuarioRepository iUsuarioRepository;


    @Override
    public List<UsuarioResponseDto> listAll() {

        return iUsuarioRepository.findAll().stream()
                .map(p -> {
                    UsuarioResponseDto usuarioResponseDto = new UsuarioResponseDto();
                    usuarioResponseDto.setId(p.getId());
                    usuarioResponseDto.setUsuario(p.getUsuario());
                    usuarioResponseDto.setClave(p.getClave());
                    usuarioResponseDto.setActive(p.getActive());
                    usuarioResponseDto.setIdCliente(p.getIdCliente());
                    RolResponseDto rolResponseDto=new RolResponseDto(p.getRol().getId(),p.getRol().getNombre());
                    usuarioResponseDto.setRol(rolResponseDto);

                   return usuarioResponseDto;
                }).collect(Collectors.toList());
    }

    @Override
    public UsuarioResponseDto save(UsuarioRequestDto request) {
        ClienteResponseDto clienteResponseDto;
        clienteResponseDto=iClienteFeing.buscar(request.getIdCliente());
        if (Objects.nonNull(clienteResponseDto.getMensaje())){
            throw new RuntimeException("No existe el cliente");
        }else{
            Usuario usuario = new Usuario();

            usuario.setUsuario(request.getUsuario());
            usuario.setClave(request.getClave());
            usuario.setActive(request.getActive());
            usuario.setIdCliente(request.getIdCliente());
            usuario.setRol(new Rol(request.getIdRol()));
            this.iUsuarioRepository.save(usuario);

            UsuarioResponseDto response = new UsuarioResponseDto();
            response.setId(usuario.getId());
            response.setUsuario(usuario.getUsuario());
            response.setClave(usuario.getClave());
            response.setActive(usuario.getActive());
            response.setIdCliente(usuario.getIdCliente());
            RolResponseDto rolResponseDto=new RolResponseDto(usuario.getRol().getId(), Roles.getRoleById(usuario.getRol().getId()));
            response.setRol(rolResponseDto);
            return response;
        }
    }

    @Override
    public UsuarioResponseDto update(UsuarioRequestDto request,Integer idUsuario) {
        Optional<Usuario> encontrado = iUsuarioRepository.findById(idUsuario);
        Usuario usuario = new Usuario();
        usuario.setId(idUsuario);
        usuario.setUsuario(request.getUsuario());
        usuario.setClave(request.getClave());
        usuario.setActive(request.getActive());
        usuario.setIdCliente(request.getIdCliente());
        usuario.setRol(new Rol(request.getIdRol()));
        this.iUsuarioRepository.save(usuario);

        UsuarioResponseDto response = new UsuarioResponseDto();
        response.setId(idUsuario);
        response.setUsuario(usuario.getUsuario());
        response.setClave(usuario.getClave());
        response.setActive(usuario.getActive());
        response.setIdCliente(usuario.getIdCliente());
        RolResponseDto rolResponseDto=new RolResponseDto(usuario.getRol().getId(), Roles.getRoleById(usuario.getRol().getId()));
        response.setRol(rolResponseDto);
        return response;
    }

    @Override
    public String eliminar(Integer id) {
        Optional<Usuario> encontrado = iUsuarioRepository.findById(id);
        if (encontrado.isPresent()) {
            iUsuarioRepository.deleteById(id);
            return "Usuario eliminado correctamente";
        }
        return "Usuario no se encuentra registrado";
    }
    @Override
    public UsuarioResponseDto getUsuarioById(Integer id) {
        UsuarioResponseDto usuarioResponseDto=new UsuarioResponseDto();
        Optional<Usuario> usuario=iUsuarioRepository.findById(id);
        if(usuario.isPresent()){
            usuarioResponseDto.setId(usuario.get().getId());
            usuarioResponseDto.setUsuario(usuario.get().getUsuario());
            usuarioResponseDto.setClave(usuario.get().getClave());
            usuarioResponseDto.setActive(usuario.get().getActive());
            usuarioResponseDto.setIdCliente(usuario.get().getIdCliente());

        }

        return usuarioResponseDto;
    }

    @Override
    public List<UsuarioResponseDto> listarUsuarioActivo(Boolean active) {
        return this.iUsuarioRepository.listUsuarioActive(active).stream()
                .map(p -> {
                    UsuarioResponseDto usuarioResponseDto = new UsuarioResponseDto();
                    usuarioResponseDto.setId(p.getId());
                    usuarioResponseDto.setUsuario(p.getUsuario());
                    usuarioResponseDto.setClave(p.getClave());
                    usuarioResponseDto.setActive(p.getActive());
                    usuarioResponseDto.setIdCliente(p.getIdCliente());

                    return usuarioResponseDto;
                }).collect(Collectors.toList());
    }


}
