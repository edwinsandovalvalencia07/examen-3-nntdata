package com.nttdata.ejsv.serviceusuario.exceptionhandler.exception;

public class ExisteEmpleado extends RuntimeException{
    public ExisteEmpleado(String message) {
        super(message);
    }
}
