package com.nttdata.ejsv.serviceusuario.dto.respose;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolResponseDto {
    private Integer id;
    private String nombre;

    public RolResponseDto(Integer id) {
        this.id = id;
    }
//    private String descripcion;
}
