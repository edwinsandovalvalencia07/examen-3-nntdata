package com.nttdata.ejsv.serviceusuario.exceptionhandler.exception;

public class ExisteEmpresa extends RuntimeException{
    public ExisteEmpresa(String message) {
        super(message);
    }
}
