package com.nttdata.ejsv.serviceusuario.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioRequestDto {
    private String usuario;
    private String clave;
    private Boolean active;
    private Integer idCliente;
    private Integer idRol;
}
