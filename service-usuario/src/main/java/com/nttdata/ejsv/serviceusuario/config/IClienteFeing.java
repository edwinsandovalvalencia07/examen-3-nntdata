package com.nttdata.ejsv.serviceusuario.config;

import com.nttdata.ejsv.serviceusuario.dto.ClienteResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
@FeignClient(name = "feingcliente",url = "${cliente-service}")
public interface IClienteFeing {
    @GetMapping("/buscar/{id}")
    public ClienteResponseDto buscar(@PathVariable("id") Integer id);
}
