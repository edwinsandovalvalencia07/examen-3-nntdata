package com.nttdata.ejsv.serviceusuario.builders;


import com.nttdata.ejsv.serviceusuario.dto.respose.RolResponseDto;
import com.nttdata.ejsv.serviceusuario.entity.Rol;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class RoleModelBuilder {

    public static Rol getRolModel(){
        Rol rolModel = new Rol();
        rolModel.setId(1);
        rolModel.setNombre("administrador");
        rolModel.setDescripcion("descripcion");

        return rolModel;
    }
    public static RolResponseDto getRolResponse(){
        RolResponseDto rolModel = new RolResponseDto();
        rolModel.setId(1);
        rolModel.setNombre("administrador");

        return rolModel;
    }

    public static List<Rol> getAll(){
        Rol re=new Rol(1,"nada","fefe");
        Rol re1=new Rol(2,"admin","fefhgt");
        Rol re2=new Rol(3,"nada","hfef");
        List<Rol> listaRols=new ArrayList<>();
        Stream.of(re,re1,re2)
                .forEach(listaRols::add);

        return listaRols;
    }
}
