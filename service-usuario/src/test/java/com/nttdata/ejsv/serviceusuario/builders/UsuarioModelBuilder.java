package com.nttdata.ejsv.serviceusuario.builders;

import com.nttdata.ejsv.serviceusuario.dto.respose.UsuarioResponseDto;
import com.nttdata.ejsv.serviceusuario.entity.Rol;
import com.nttdata.ejsv.serviceusuario.entity.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class UsuarioModelBuilder {

    public static Usuario getUsuarioModel(){
        Usuario usuarioModel = new Usuario();
        usuarioModel.setId(1);
        usuarioModel.setUsuario("Sandoval");
        usuarioModel.setClave("89563212");
        usuarioModel.setActive(true);
        usuarioModel.setRol(RoleModelBuilder.getRolModel());
        usuarioModel.setIdCliente(1);

        return usuarioModel;
    }

    public static List<Usuario> getAll(){
        Usuario u= new Usuario(1,"VALENCIA","123",true,1,RoleModelBuilder.getRolModel());
        Usuario u1= new Usuario(2,"YESQUEN","123",true,1,RoleModelBuilder.getRolModel());
        Usuario u2= new Usuario(3,"SOSA","123",true,1,RoleModelBuilder.getRolModel());

        List<Usuario> listaRols=new ArrayList<>();
        Stream.of(u,u1,u2)
                .forEach(listaRols::add);

        return listaRols;
    }
    public static List<UsuarioResponseDto> getAllResponse(){
        UsuarioResponseDto u= new UsuarioResponseDto(1,"VALENCIA","123",true,1,RoleModelBuilder.getRolResponse());
        UsuarioResponseDto u1= new UsuarioResponseDto(2,"YESQUEN","123",true,1,RoleModelBuilder.getRolResponse());
        UsuarioResponseDto u2= new UsuarioResponseDto(3,"SOSA","123",true,1,RoleModelBuilder.getRolResponse());

        List<UsuarioResponseDto> listaRols=new ArrayList<>();
        Stream.of(u,u1,u2)
                .forEach(listaRols::add);

        return listaRols;
    }
}
