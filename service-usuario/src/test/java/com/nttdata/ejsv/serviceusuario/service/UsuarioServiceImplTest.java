package com.nttdata.ejsv.serviceusuario.service;

import com.nttdata.ejsv.serviceusuario.builders.UsuarioModelBuilder;
import com.nttdata.ejsv.serviceusuario.config.IClienteFeing;
import com.nttdata.ejsv.serviceusuario.dto.ClienteResponseDto;
import com.nttdata.ejsv.serviceusuario.dto.request.UsuarioRequestDto;
import com.nttdata.ejsv.serviceusuario.dto.respose.UsuarioResponseDto;
import com.nttdata.ejsv.serviceusuario.entity.Rol;
import com.nttdata.ejsv.serviceusuario.entity.Usuario;
import com.nttdata.ejsv.serviceusuario.repository.IUsuarioRepository;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
//@RunWith(SpringRunner.class)
@SpringBootTest
class UsuarioServiceImplTest {


    @InjectMocks
    private UsuarioServiceImpl usuarioService;

    @Mock
    private IClienteFeing clienteFeing;

    @Mock
    private IUsuarioRepository usuarioRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testlistAll() {
        List<Usuario> esperado=UsuarioModelBuilder.getAll();
        when(usuarioRepository.findAll()).thenReturn(esperado);
        List<UsuarioResponseDto> resultado = usuarioService.listAll();
        assertEquals(3,resultado.size());
        // Agrega más aserciones según tus requerimientos
        // Por ejemplo, verifica que los datos mapeados sean correctos
        // Puedes comparar elementos individuales del resultado con los datos simulados
        assertEquals("VALENCIA", resultado.get(0).getUsuario());
        assertEquals("YESQUEN", resultado.get(1).getUsuario());
    }

    @Test
    void testSave_ClienteExistente() {
        // Configura datos simulados para el cliente
        ClienteResponseDto clienteResponseDto = new ClienteResponseDto();
        clienteResponseDto.setId(1);
        clienteResponseDto.setMensaje(null);

        // Configura datos simulados para el usuario
        UsuarioRequestDto request = new UsuarioRequestDto();
        request.setIdCliente(1);
        request.setUsuario("Usuario1");
        request.setClave("Clave1");
        request.setActive(true);
        request.setIdRol(1);

        // Configura el comportamiento esperado de las dependencias simuladas
        when(clienteFeing.buscar(1)).thenReturn(clienteResponseDto);
        when(usuarioRepository.save(any(Usuario.class))).thenReturn(new Usuario());

        // Llama al método que deseas probar
        UsuarioResponseDto resultado = usuarioService.save(request);

        // Verifica el resultado
        assertNotNull(resultado);
        assertEquals("Usuario1", resultado.getUsuario());

    }

    @Test
    void testUpdate_UsuarioExistente() {
        Usuario usuarioExistente = new Usuario();
        usuarioExistente.setId(1);
        usuarioExistente.setUsuario("Usuario1");
        usuarioExistente.setClave("Clave1");
        usuarioExistente.setActive(true);
        usuarioExistente.setIdCliente(1);
        usuarioExistente.setRol(new Rol(1));

        // Configura datos simulados para la solicitud de actualización
        UsuarioRequestDto request = new UsuarioRequestDto();
        request.setUsuario("NuevoUsuario");
        request.setClave("NuevaClave");
        request.setActive(false);
        request.setIdCliente(2);
        request.setIdRol(2);

        // Configura el comportamiento esperado de las dependencias simuladas
        when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuarioExistente));
        when(usuarioRepository.save(any(Usuario.class))).thenReturn(usuarioExistente);

        // Llama al método que deseas probar
        UsuarioResponseDto resultado = usuarioService.update(request, 1);

        // Verifica el resultado
        assertNotNull(resultado);
        assertEquals("NuevoUsuario", resultado.getUsuario());
        assertEquals("NuevaClave", resultado.getClave());
        assertFalse(resultado.getActive());
        assertEquals(2, resultado.getIdCliente());
        assertEquals(2, resultado.getRol().getId());
        // Agrega más aserciones según tus requerimientos
    }

    @Test
    void testEliminar_UsuarioExistente() {

        // Configura el comportamiento esperado de las dependencias simuladas para un usuario existente
        Usuario esperado=UsuarioModelBuilder.getUsuarioModel();
        when(usuarioRepository.findById(1)).thenReturn(Optional.of(esperado));

        // Llama al método que deseas probar
        String resultado = usuarioService.eliminar(1);

        // Verifica el resultado
        assertEquals("Usuario eliminado correctamente", resultado);
    }
//
//    @Test
//    void getUsuarioById() {
//    }
//
    @Test
    void testListarUsuarioActivo_Activos() {
        List<Usuario> esperado = UsuarioModelBuilder.getAll();
        // Configura el comportamiento esperado de las dependencias simuladas
        when(usuarioRepository.listUsuarioActive(true)).thenReturn(esperado);
        // Llama al método que deseas probar
        List<UsuarioResponseDto> resultado = usuarioService.listarUsuarioActivo(true);
        // Verifica el resultado
        assertNotNull(resultado);
        assertEquals(3, resultado.size());
        assertEquals("VALENCIA", resultado.get(0).getUsuario());
    }
}