package com.nttdata.ejsv.cuentaBancaria.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CuentaBancariaResponseDto {
//    private Integer id;
    private String numeroCuenta;
    private String tipoCuenta;
    private Integer idBanco;
    private Integer idCliente;
    private Boolean active;
}
