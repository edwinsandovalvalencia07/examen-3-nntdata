package com.nttdata.ejsv.cuentaBancaria.utils;

public enum TipoCuenta {
    CUENTA_CORRIENTE,
    CUENTA_DE_AHORRO
}
