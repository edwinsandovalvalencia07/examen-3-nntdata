package com.nttdata.ejsv.cuentaBancaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class CuentaBancariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuentaBancariaApplication.class, args);
	}

}
