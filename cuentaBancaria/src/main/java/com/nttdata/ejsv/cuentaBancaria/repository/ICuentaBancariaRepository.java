package com.nttdata.ejsv.cuentaBancaria.repository;


import com.nttdata.ejsv.cuentaBancaria.entity.CuentaBancaria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICuentaBancariaRepository extends JpaRepository<CuentaBancaria, Integer> {
//    @Query(value = "select c from CunetaBancaria c where c.numero_cuenta LIKE ?1")
//    CuentaBancaria buscarPorNumeroCuenta(String nroCuenta);
}
